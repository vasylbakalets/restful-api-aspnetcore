﻿using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Library.API.Helpers.Extensions;
using Library.API.Services.PropertyMappingService;
using Mapper = AutoMapper.Mapper;

namespace Library.API.Controllers
{
    [Route("api/authors")]
    public class AuthorsController : Controller
    {
        public AuthorsController(ILibraryRepository libraryRepository, IUrlHelper urlHelper, 
            IPropertyMappingService propertyMappingService, ITypeHelperService typeHelperService)
        {
            _libraryRepository = libraryRepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
        }

        [HttpGet(Name = "GetAuthors")]
        [HttpHead]
        public IActionResult GetAutors(AuthorsResourceParameters authorsResourceParams,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            if (!_propertyMappingService.ValidMappingExistsFor<AuthorDto, Author>(authorsResourceParams.OrderBy))
            {
                return BadRequest();
            }

            if (!_typeHelperService.TypeHasProperties<AuthorDto>(authorsResourceParams.Fields))
            {
                return BadRequest();
            }

            var authors = _libraryRepository.GetAuthors(authorsResourceParams);

            var shapedAuthors = Mapper.Map<IEnumerable<AuthorDto>>(authors)
                .ShapeData(authorsResourceParams.Fields);

            var isCustomMediaType = IsCustomMediaType(mediaType);
            AddPaginationToResponseHeaders(authors, authorsResourceParams, isCustomMediaType);

            if (!isCustomMediaType)
            {
                return Ok(shapedAuthors);
            }

            var shapedAuthorsWithLinks = shapedAuthors.Select(author =>
            {
                var authorAsDictionary = author as IDictionary<string, object>;
                var authorLinks = CreateLinksForAuthor(
                    (Guid) authorAsDictionary["Id"], authorsResourceParams.Fields);

                authorAsDictionary.Add("links", authorLinks);
                return authorAsDictionary;
            });

            return Ok(new
            {
                value = shapedAuthorsWithLinks,
                links = CreateLinksForAuthors(authorsResourceParams,
                    authors.HasNext, authors.HasPrevious)
            });
        }

        [HttpGet("{id}", Name = "GetAuthor")]
        public IActionResult GetAutor(Guid id, [FromQuery] string fields,
            [FromHeader] string mediaType)
        {
            var author = _libraryRepository.GetAuthor(id);
            if (author == null)
            {
                return NotFound();
            }

            if (!_typeHelperService.TypeHasProperties<AuthorDto>(fields))
            {
                return BadRequest();
            }

            var shapedAuthor = Mapper.Map<AuthorDto>(author).ShapeData(fields);

            if (!IsCustomMediaType(mediaType))
            {
                return Ok(shapedAuthor);
            }

            var links = CreateLinksForAuthor(id, fields);
            var linkedAuthor = (IDictionary<string, object>) shapedAuthor;
            linkedAuthor.Add("links", links);

            return Ok(linkedAuthor);
        }

        [HttpPost(Name = "CreateAuthor")]
        [RequestHeaderMatchesMediaType("Content-Type",
            new[] {"application/vnd.wsb.author.full+json",
                "application/vnd.wsb.author.full+xml" })]
        public IActionResult CreateAuthor([FromBody] CreateAuthorDto createAuthor,
            [FromHeader] string mediaType)
        {
            if (createAuthor == null)
            {
                return BadRequest();
            }

            var author = Mapper.Map<Author>(createAuthor);
            _libraryRepository.AddAuthor(author);

            if (!_libraryRepository.Save())
            {
                throw new Exception("failed to create author");
            }

            var shapedAuthor = Mapper.Map<AuthorDto>(author).ShapeData(null);

            if (!IsCustomMediaType(mediaType))
            {
                return CreatedAtRoute("GetAuthor",
                    new {id = author.Id},
                    shapedAuthor);
            }

            var links = CreateLinksForAuthor(author.Id, null);
            var linkedAuthor = (IDictionary<string, object>) shapedAuthor;
            linkedAuthor.Add("links", links);

            return CreatedAtRoute("GetAuthor",
                new { id = linkedAuthor["Id"] },
                linkedAuthor);
        }

        [HttpPost(Name = "CreateAuthorWithDateOfDeath")]
        [RequestHeaderMatchesMediaType("Content-Type",
            new[] { "application/vnd.wsb.authorwithdateofdeath.full+json",
                "application/vnd.wsb.authorwithdateofdeath.full+xml" })]
        public IActionResult CreateAuthorWithDateOfDeath(
            [FromBody] CreateAuthorWithDateOfDeathDto createAuthor,
            [FromHeader] string mediaType)
        {
            if (createAuthor == null)
            {
                return BadRequest();
            }
            // specific validations for create author with date of death
            // goes here

            var author = Mapper.Map<Author>(createAuthor);
            _libraryRepository.AddAuthor(author);

            if (!_libraryRepository.Save())
            {
                throw new Exception("failed to create author");
            }

            var shapedAuthor = Mapper.Map<AuthorDto>(author).ShapeData(null);

            if (!IsCustomMediaType(mediaType))
            {
                return CreatedAtRoute("GetAuthor",
                    new { id = author.Id },
                    shapedAuthor);
            }

            var links = CreateLinksForAuthor(author.Id, null);
            var linkedAuthor = (IDictionary<string, object>) shapedAuthor;
            linkedAuthor.Add("links", links);

            return CreatedAtRoute("GetAuthor",
                new { id = linkedAuthor["Id"] },
                linkedAuthor);
        }

        [HttpPost("{id}")]
        public IActionResult BlockAuthorCreation(Guid id)
        {
            if (_libraryRepository.AuthorExists(id))
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            return NotFound();
        }

        [HttpDelete("{id}", Name = "DeleteAuthor")]
        public IActionResult DeleteAutor(Guid id)
        {
            var author = _libraryRepository.GetAuthor(id);
            if (author == null)
            {
                return NotFound();
            }

            _libraryRepository.DeleteAuthor(author);
            if (!_libraryRepository.Save())
            {
                throw new Exception($"failed to delete author {id}");
            }

            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetAuthorsOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST");
            return Ok();
        }


        private string CreateAuthorsResourceUri(AuthorsResourceParameters authorsResourceParams,
            ResourceUriType resourceType)
        {
            switch (resourceType)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetAuthors",
                      new
                      {
                          fields = authorsResourceParams.Fields,
                          orderBy = authorsResourceParams.OrderBy,
                          searchQuery = authorsResourceParams.SearchQuery,
                          genre = authorsResourceParams.Genre,
                          pageNumber = authorsResourceParams.PageNumber - 1,
                          pageSize = authorsResourceParams.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetAuthors",
                      new
                      {
                          fields = authorsResourceParams.Fields,
                          orderBy = authorsResourceParams.OrderBy,
                          searchQuery = authorsResourceParams.SearchQuery,
                          genre = authorsResourceParams.Genre,
                          pageNumber = authorsResourceParams.PageNumber + 1,
                          pageSize = authorsResourceParams.PageSize
                      });

                default:
                    return _urlHelper.Link("GetAuthors",
                    new
                    {
                        fields = authorsResourceParams.Fields,
                        orderBy = authorsResourceParams.OrderBy,
                        searchQuery = authorsResourceParams.SearchQuery,
                        genre = authorsResourceParams.Genre,
                        pageNumber = authorsResourceParams.PageNumber,
                        pageSize = authorsResourceParams.PageSize
                    });
            }
        }

        private IEnumerable<LinkDto> CreateLinksForAuthor(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAuthor", new { id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAuthor", new { id, fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteAuthor", new { id }),
              "delete_author",
              "DELETE"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateBookForAuthor", new { authorId = id }),
              "create_book_for_author",
              "POST"));

            links.Add(
               new LinkDto(_urlHelper.Link("GetBooksForAuthor", new { authorId = id }),
               "books",
               "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAuthors(
            AuthorsResourceParameters authorsResourceParameters,
            bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>
            {
                new LinkDto(CreateAuthorsResourceUri(authorsResourceParameters,
                    ResourceUriType.Current),
                    "self",
                    "GET")
            };

            if (hasNext)
            {
                links.Add(new LinkDto(CreateAuthorsResourceUri(authorsResourceParameters,
                    ResourceUriType.NextPage),
                    "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateAuthorsResourceUri(authorsResourceParameters,
                    ResourceUriType.PreviousPage),
                    "previousPage", "GET"));
            }

            return links;
        }

        private void AddPaginationToResponseHeaders(PagedList<Author> authors,
            AuthorsResourceParameters authorsResourceParams, bool isCustomMediaType)
        {
            object paginationMetadata;
            if (isCustomMediaType)
            {
                paginationMetadata = new
                {
                    totalCount = authors.TotalCount,
                    pageSize = authors.PageSize,
                    currentPage = authors.CurrentPage,
                    totalPages = authors.TotalPages
                };
            }
            else
            {
                paginationMetadata = new
                {
                    totalCount = authors.TotalCount,
                    pageSize = authors.PageSize,
                    currentPage = authors.CurrentPage,
                    totalPages = authors.TotalPages,
                    previousPageLink = authors.HasPrevious ?
                        CreateAuthorsResourceUri(authorsResourceParams,
                            ResourceUriType.PreviousPage) : null,
                    nextPageLink = authors.HasNext ?
                        CreateAuthorsResourceUri(authorsResourceParams,
                            ResourceUriType.NextPage) : null
                };
            }

            Response.Headers.Add("X-Pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));
        }

        private static bool IsCustomMediaType(string mediaType)
        {
            return mediaType == "application/vnd.wsb.hateoas+json";
        }


        private readonly ILibraryRepository _libraryRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly ITypeHelperService _typeHelperService;
    }
}