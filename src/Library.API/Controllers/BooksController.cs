﻿using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Mapper = AutoMapper.Mapper;

namespace Library.API.Controllers
{
    [Route("api/authors/{authorId}/books")]
    public class BooksController : Controller
    {
        public BooksController(ILibraryRepository libraryRepository,
            ILogger<BooksController> logger, IUrlHelper urlHelper)
        {
            _libraryRepository = libraryRepository;
            _logger = logger;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetBooksForAuthor")]
        public IActionResult GetBooksForAutor(Guid authorId)
        {
            if (!_libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var books = _libraryRepository.GetBooksForAuthor(authorId);

            var booksWithLinks = Mapper.Map<IEnumerable<BookDto>>(books)
                .Select(CreateLinksForBook);

            var wrapper = new LinkedCollectionResourceWrapperDto<BookDto>(booksWithLinks);
            var booksToReturn = CreateLinksForBooks(wrapper);

            return Ok(booksToReturn);
        }

        [HttpGet("{id}", Name = "GetBookForAuthor")]
        public IActionResult GetBookForAutor(Guid authorId, Guid id)
        {
            if (!_libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var book = _libraryRepository.GetBookForAuthor(authorId, id);
            if (book == null)
            {
                return NotFound();
            }

            var bookToReturn = CreateLinksForBook(Mapper.Map<BookDto>(book));

            return Ok(bookToReturn);
        }

        [HttpPost(Name = "CreateBookForAuthor")]
        public IActionResult CreateBookForAuthor(Guid authorId,
            [FromBody] CreateBookDto createBook)
        {
            if (createBook == null)
            {
                return BadRequest();
            }

            if (createBook.Description == createBook.Title)
            {
                ModelState.AddModelError(nameof(CreateBookDto),
                    "The provided description should be different from the title.");
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!_libraryRepository.AuthorExists(authorId))
            {
                return BadRequest();
            }

            var book = Mapper.Map<Book>(createBook);

            _libraryRepository.AddBookForAuthor(authorId, book);

            if (!_libraryRepository.Save())
            {
                throw new Exception("failed to save book");
            }

            var bookToReturn = CreateLinksForBook(Mapper.Map<BookDto>(book));

            return CreatedAtRoute("GetBookForAuthor",
                new {id = bookToReturn.Id },
                bookToReturn);
        }

        [HttpDelete("{id}", Name = "DeleteBookForAuthor")]
        public IActionResult DeleteBookForAuthor(Guid authorId, Guid id)
        {
            if (!_libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var book = _libraryRepository.GetBookForAuthor(authorId, id);
            if (book == null)
            {
                return NotFound();
            }

            _libraryRepository.DeleteBook(book);

            if (!_libraryRepository.Save())
            {
                throw new Exception($"failed delete the book {id} for author {authorId}");
            }

            _logger.LogInformation(100, $"book {id} was deleted for author {authorId}");

            return NoContent();
        }

        [HttpPut("{id}", Name = "UpdateBookForAuthor")]
        public IActionResult UpdateBookForAuthor(Guid authorId, Guid id,
            [FromBody] UpdateBookDto updateBook)
        {
            if (updateBook == null)
            {
                return BadRequest();
            }

            if (updateBook.Description == updateBook.Title)
            {
                ModelState.AddModelError(nameof(UpdateBookDto),
                    "The provided description should be different from the title.");
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!_libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var book = _libraryRepository.GetBookForAuthor(authorId, id);
            if (book == null)
            {
                //upsert
                var bookToAdd = Mapper.Map<Book>(updateBook);
                bookToAdd.Id = id;

                _libraryRepository.AddBookForAuthor(authorId, bookToAdd);

                if (!_libraryRepository.Save())
                {
                    throw new Exception($"failed to upsert book {id} for author {authorId}");
                }

                var bookToReturn = Mapper.Map<BookDto>(bookToAdd);

                return CreatedAtRoute("GetBookForAuthor", new {id = bookToReturn.Id}, bookToReturn);
            }

            Mapper.Map(updateBook, book);
            _libraryRepository.UpdateBookForAuthor(book);

            if (!_libraryRepository.Save())
            {
                throw new Exception($"failed to update book {id} for author {authorId}");
            }

            return NoContent();
        }

        [HttpPatch("{id}", Name = "PartiallyUpdateBookForAuthor")]
        public IActionResult PartiallyUpdateBookForAuthor(Guid authorId, Guid id,
            [FromBody] JsonPatchDocument<UpdateBookDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var book = _libraryRepository.GetBookForAuthor(authorId, id);
            if (book == null)
            {
                var updateBook = new UpdateBookDto();
                patchDoc.ApplyTo(updateBook, ModelState);

                if (updateBook.Description == updateBook.Title)
                {
                    ModelState.AddModelError(nameof(UpdateBookDto),
                        "The provided description should be different from the title.");
                }

                TryValidateModel(updateBook);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                var bookToAdd = Mapper.Map<Book>(updateBook);
                bookToAdd.Id = id;

                _libraryRepository.AddBookForAuthor(authorId, bookToAdd);

                if (!_libraryRepository.Save())
                {
                    throw new Exception($"failed to upsert book {id} for author {authorId}");
                }

                var bookToReturn = Mapper.Map<BookDto>(bookToAdd);
                return CreatedAtRoute("GetBookForAuthor",
                    new { authorId, id = bookToReturn.Id},
                    bookToReturn);
            }

            var bookToPatch = Mapper.Map<UpdateBookDto>(book);
            patchDoc.ApplyTo(bookToPatch, ModelState);

            if (bookToPatch.Description == bookToPatch.Title)
            {
                ModelState.AddModelError(nameof(UpdateBookDto),
                    "The provided description should be different from the title.");
            }

            TryValidateModel(bookToPatch);

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            Mapper.Map(bookToPatch, book);

            _libraryRepository.UpdateBookForAuthor(book);

            if (!_libraryRepository.Save())
            {
                throw new Exception($"failed to patch book {id} for author {authorId}");
            }

            return NoContent();
        }


        private BookDto CreateLinksForBook(BookDto book)
        {
            book.Links.Add(new LinkDto(_urlHelper.Link("GetBookForAuthor",
                new { id = book.Id }),
                "self",
                "GET"));

            book.Links.Add(new LinkDto(_urlHelper.Link("DeleteBookForAuthor",
                new { id = book.Id }),
                "delete_book",
                "DELETE"));

            book.Links.Add(new LinkDto(_urlHelper.Link("UpdateBookForAuthor",
                new { id = book.Id }),
                "update_book",
                "PUT"));

            book.Links.Add(new LinkDto(_urlHelper.Link("PartiallyUpdateBookForAuthor",
                new { id = book.Id }),
                "partially_update_book",
                "PATCH"));

            return book;
        }

        private LinkedCollectionResourceWrapperDto<BookDto> CreateLinksForBooks(
            LinkedCollectionResourceWrapperDto<BookDto> booksWrapper)
        {
            booksWrapper.Links.Add(new LinkDto(_urlHelper.Link("GetBooksForAuthor", 
                new { }),
                "self",
                "GET"));

            return booksWrapper;
        }


        private readonly ILibraryRepository _libraryRepository;
        private readonly ILogger<BooksController> _logger;
        private readonly IUrlHelper _urlHelper;
    }
}